@extends('layouts.app')


@section('css')
<link   rel="stylesheet" href="/css/materialInputs.css"/>
<style>
.input_icon{
    padding: 1rem;
    border-bottom: 1px solid;
    padding-bottom: 0;
}
.cuisine_label{
    font-size:16px;
}
.item_rating{
    font-size: 20px;
}
.item_cuisine{
    font-size: 20px;
    margin-left: 1rem;
}
.page_title{
            font-size:36px;
        }
.card_brd{
    border-radius:10px !important;
}

.card_img_brd{
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;

}
</style>
@endsection

@section('body')

<section class="">
        <div class="row align-items-stretch">
                <div class="col-lg-12 g-mb-50">
                    <!-- Article -->
                    <article class="text-center g-color-white g-overflow-hidden">
                        <div class="g-min-height-200 g-flex-middle g-bg-cover g-bg-size-cover g-bg-bluegray-opacity-0_3--after g-transition-0_5" data-bg-img-src="/img/banner4.jpg">
                            <div class="g-flex-middle-item g-pos-rel g-z-index-1 g-pb-50 g-pt-50 g-px-20">
                                <h3 class="page_title">Restaurants</h3>
                            </div>
                        </div>
                    </article>
                    <!-- End Article -->
                </div>
                
                
            </div>
    <div class="g-mx-100">
        <div class="row">
            <div class="col-md-3">
                    <div class="mb-3">
                        <div class="input-group">
                        <span class="input_icon"><i class="fa fa-location-arrow"></i></span>
                        <input class="form-control g-brd-none g-brd-bottom g-brd-black g-brd-primary--focus g-color-black g-bg-transparent rounded-0 g-py-13 g-px-0" type="text" placeholder="Select Area" value="Sydney">
                        </div>
                    </div>

                    <div class="mb-4">
                        <div class="input-group">
                        <span class="input_icon"><i class="fa fa-clock"></i></span>
                        <select style="height:3rem" class="form-control g-brd-none g-brd-bottom g-brd-black g-brd-primary--focus g-color-black g-bg-transparent g-placeholder-gray-light-v5 rounded-0 g-py-13 g-px-0">
                            <option>Breakfast</option>
                            <option>Lunch</option>
                            <option>Dinner</option>
                        </select>
                        </div>
                    </div>

                 
                    <hr>

                    <div class="g-my-30">
                            <h3 class="h5 g-font-size-20 g-font-weight-600 mb-3">Cuisine</h3>
            
                        <ul class="list-unstyled">
                            <li class="my-2">
                                <label class="form-check-inline cuisine_label u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon=""></i>
                                    </span>
                                    Thai
                                </label>
                            </li>
                            <li class="my-2">
                                <label class="form-check-inline cuisine_label u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" checked="">
                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon=""></i>
                                    </span>
                                  Chinese 
                                </label>
                            </li>
                            <li class="my-2">
                                <label class="form-check-inline cuisine_label u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon=""></i>
                                    </span>
                                    Indian 
                                </label>
                            </li>
                            <li class="my-2">
                                <label class="form-check-inline cuisine_label u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon=""></i>
                                    </span>
                                    Lebanese
                                </label>
                            </li>
                            <li class="my-2">
                                <label class="form-check-inline cuisine_label u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" checked="">
                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon=""></i>
                                    </span>
                                  Japanese 
                                </label>
                            </li>
                            <li class="my-2">
                                <label class="form-check-inline cuisine_label u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon=""></i>
                                    </span>
                                    Vietnamese 
                                </label>
                            </li>
                            <li class="my-2">
                                <label class="form-check-inline cuisine_label u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon=""></i>
                                    </span>
                                    Mexican
                                </label>
                            </li>
                            <li class="my-2">
                                <label class="form-check-inline cuisine_label u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" checked="">
                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon=""></i>
                                    </span>
                                  Korean 
                                </label>
                            </li>
                            <li class="my-2">
                                <label class="form-check-inline cuisine_label u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                        <i class="fa" data-check-icon=""></i>
                                    </span>
                                    European 
                                </label>
                            </li>
                        </ul>
                    </div>
            </div>
            <div class="col-md-9">
                    <div class="card-group row g-mx-minus-15">
                        <div class="col-md-4 p-0">
                            <div class="card u-shadow-v36 card_brd g-mx-15 g-mb-30">
                                <!-- Research Article -->
                                <article>
                                
                                    <div class="g-pos-rel">
                                        <img class="img-fluid card_img_brd"  src="/img/restaurant1.jpg" alt="Image Description">
                                        <figcaption class="g-pos-abs" style="    bottom: -13px; right: 10px;">
                                            <a class="btn btn-sm u-btn-red rounded-10" href="#">Reserve</a>
                                        </figcaption>
                                    </div>
                                    <div class="card-body card_brd g-bg-white g-pa-20">
                                        <h3 class="h4 mb-0">Itihas Indian Restaurant</h3>
                                        <span class="item_rating"><i class="fas fa-star" style="color:#ff9d00"></i><i class="fas fa-star" style="color:#ff9d00"></i><i class="fas fa-star" style="color:#ff9d00"></i><i class="far fa-star" style="color:#ff9d00"></i><i class="far fa-star" style="color:#ff9d00"></i></span>
                                      
                                        <span class="item_cuisine">Indian</span>
                                    </div>
                                </article>
                                <!-- End Research Article -->
                            </div>
                        </div>
                        <div class="col-md-4 p-0">

                            <div class="card u-shadow-v36 card_brd g-mx-15 g-mb-30">
                                <!-- Research Article -->
                                <article>
                                    <div class="g-pos-rel">
                                        <img class="img-fluid card_img_brd" src="/img/restaurant2.jpg" alt="Image Description">
                                        <figcaption class="g-pos-abs" style="    bottom: -13px; right: 10px;">
                                            <a class="btn btn-sm u-btn-red rounded-10" href="#">Reserve</a>
                                        </figcaption>
                                    </div>
                                    <div class="card-body card_brd g-bg-white g-pa-20">
                                        <h3 class="h4 mb-0">Delhi O Delhi </h3>
                                        <span class="item_rating"><i class="fas fa-star" style="color:#ff9d00"></i><i class="fas fa-star" style="color:#ff9d00"></i><i class="fas fa-star" style="color:#ff9d00"></i><i class="fas fa-star" style="color:#ff9d00"></i><i class="fas fa-star" style="color:#ff9d00"></i></span>
                                        <span class="item_cuisine">Indian</span>
                                    </div>
                                </article>
                                <!-- End Research Article -->
                            </div>
                        </div>

                        <div class="col-md-4 p-0">

                            
                            <div class="card u-shadow-v36 card_brd g-mx-15 g-mb-30">
                                <!-- Research Article -->
                                <article>
                                    <div class="g-pos-rel">
                                        <img class="img-fluid card_img_brd" src="/img/restaurant3.jpg" alt="Image Description">
                                        <figcaption class="g-pos-abs" style="    bottom: -13px; right: 10px;">
                                            <a class="btn btn-sm u-btn-red rounded-10" href="#">Reserve</a>
                                        </figcaption>
                                    </div>
                                    <div class="card-body g-bg-white  g-pa-20">
                                            <h3 class="h4 mb-0">Delhi O Delhi </h3>
                                            <span class="item_rating"><i class="fas fa-star" style="color:#ff9d00"></i><i class="fas fa-star" style="color:#ff9d00"></i><i class="fas fa-star" style="color:#ff9d00"></i><i class="far fa-star" style="color:#ff9d00"></i><i class="far fa-star" style="color:#ff9d00"></i></span>                                           
                                            <span class="item_cuisine"> Indian</span>
                                    
                                    </div>
                                </article>
                                <!-- End Research Article -->
                            </div>
                        </div>
                        <div class="col-md-4 p-0">

                            <div class="card u-shadow-v36 card_brd g-mx-15 g-mb-30">
                                <!-- Research Article -->
                                <article>
                                    <div class="g-pos-rel">
                                        <img class="img-fluid card_img_brd" src="/img/restaurant2.jpg" alt="Image Description">
                                        <figcaption class="g-pos-abs" style="    bottom: -13px; right: 10px;">
                                            <a class="btn btn-sm u-btn-red rounded-10" href="#">Reserve</a>
                                        </figcaption>
                                    </div>
                                    <div class="card-body card_brd g-bg-white g-pa-20">
                                        <h3 class="h4 mb-0">Delhi O Delhi </h3>
                                        <span class="item_rating"><i class="fas fa-star" style="color:#ff9d00"></i><i class="fas fa-star" style="color:#ff9d00"></i><i class="fas fa-star" style="color:#ff9d00"></i><i class="fas fa-star" style="color:#ff9d00"></i><i class="fas fa-star" style="color:#ff9d00"></i></span>
                                        <span class="item_cuisine">Indian</span>
                                    </div>
                                </article>
                                <!-- End Research Article -->
                            </div>
                        </div>
                        <div class="col-md-4 p-0">

                            
                            <div class="card u-shadow-v36 card_brd g-mx-15 g-mb-30">
                                <!-- Research Article -->
                                <article>
                                    <div class="g-pos-rel">
                                        <img class="img-fluid card_img_brd" src="/img/restaurant3.jpg" alt="Image Description">
                                        <figcaption class="g-pos-abs" style="    bottom: -13px; right: 10px;">
                                            <a class="btn btn-sm u-btn-red rounded-10" href="#">Reserve</a>
                                        </figcaption>
                                    </div>
                                    <div class="card-body g-bg-white  g-pa-20">
                                            <h3 class="h4 mb-0">Delhi O Delhi </h3>
                                            <span class="item_rating"><i class="fas fa-star" style="color:#ff9d00"></i><i class="fas fa-star" style="color:#ff9d00"></i><i class="fas fa-star" style="color:#ff9d00"></i><i class="far fa-star" style="color:#ff9d00"></i><i class="far fa-star" style="color:#ff9d00"></i></span>                                           
                                            <span class="item_cuisine"> Indian</span>
                                    
                                    </div>
                                </article>
                                <!-- End Research Article -->
                            </div>
                        </div>
                        <div class="col-md-4 p-0">

                            <div class="card u-shadow-v36 card_brd g-mx-15 g-mb-30">
                                <!-- Research Article -->
                                <article>
                                    <div class="g-pos-rel">
                                        <img class="img-fluid card_img_brd" src="/img/restaurant2.jpg" alt="Image Description">
                                        <figcaption class="g-pos-abs" style="    bottom: -13px; right: 10px;">
                                            <a class="btn btn-sm u-btn-red rounded-10" href="#">Reserve</a>
                                        </figcaption>
                                    </div>
                                    <div class="card-body card_brd g-bg-white g-pa-20">
                                        <h3 class="h4 mb-0">Delhi O Delhi </h3>
                                        <span class="item_rating"><i class="fas fa-star" style="color:#ff9d00"></i><i class="fas fa-star" style="color:#ff9d00"></i><i class="fas fa-star" style="color:#ff9d00"></i><i class="fas fa-star" style="color:#ff9d00"></i><i class="fas fa-star" style="color:#ff9d00"></i></span>
                                        <span class="item_cuisine">Indian</span>
                                    </div>
                                </article>
                                <!-- End Research Article -->
                            </div>
                        </div>

                    </div>
            </div>
        </div>
    </div>
</section>

@endsection