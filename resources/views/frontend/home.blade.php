@extends('layouts.app')


@section('css')

@endsection
@section('body')

<?php
$colors=collect(['rgb(235,21,54)','rgb(235,130,21)','rgb(29,162,20)']);
$color=$colors->random()
// dd($colors->random());
?>
<style>
  /* input[type=text] {
  
} */
.search{
  width: 100%;
  border-color: white;
  background: transparent;
  line-height:40px;
    border: none;
    font-size:20px;
    border-bottom: 2px solid #fff;
    margin-top:1rem;
    color:white !important;
  /* border-width:2px; */
}
.search:focus{
  outline:none;
}
.search_text{
  font-size:20px;
  color:white;
  margin-top:1rem;
  font-weight:500;
  /* text-transform: uppercase */

}
.key_icon{

  font-size:40px;
  font-weight:200;
  color:white;
}
::placeholder {
  color: white;
  opacity: 1; /* Firefox */
  font-weight: 100;
  
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
 color: white;
 font-weight: 100;

}

::-ms-input-placeholder { /* Microsoft Edge */
 color: white;
 font-weight: 100;

}
.landing_title{
  font-size: 35px;
    margin-bottom: 2rem;
    font-weight: 600;
    color: black;
}


.landing_description{
  margin-top: 2rem;
    font-size: 18px;
    text-align:justify;
}
.landing_a{
  font-size: 18px;
    color: #2384aa;
}
.diagonal-split-background{
  background-color: {{$color}};
  background-image: -webkit-linear-gradient(140deg, {{$color}} 75%, #6868d4 20%);
}
.partner_left_text{
  color: white;
    margin-left: 5rem;
    font-size: 27px;
    margin-bottom: 0px;
    line-height: 31px;
}
.partner_right_text{
  font-size: 30px;
    color: white;
}

</style>
      <div id="rev_slider_347_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="parallaxjoy" data-source="gallery" style="background:linear-gradient(45deg, rgba(82,82,212,1) 0%, rgba(136,136,212,1) 100%);padding:0px;">
        <!-- START REVOLUTION SLIDER 5.4.3.3 fullscreen mode -->
        <div id="rev_slider_347_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.3.3">
          <ul>  <!-- SLIDE  -->
            <li data-index="rs-967" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="1000" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
              <!-- MAIN IMAGE -->
              <img src="/frontend-assets/assets/img/transparent.png" data-bgcolor="linear-gradient(45deg, rgba(82,82,212,1) 0%, rgba(136,136,212,1) 100%)" style="background:linear-gradient(45deg, rgba(82,82,212,1) 0%, rgba(136,136,212,1) 100%)" alt="Image description" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
              <!-- LAYERS -->

              <!-- LAYER NR. 1 -->
              <div class="tp-caption   tp-resizeme"
                   id="slide-967-layer-25"
                   data-x="['left','left','left','left']" data-hoffset="['21','21','21','-9']"
                   data-y="['top','top','top','top']" data-voffset="['45','45','45','30']"
                   data-width="none"
                   data-height="none"
                   data-whitespace="nowrap"

                   data-type="image"
                   data-responsive_offset="on"

                   data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:360deg;opacity:0;","to":"o:1;rZ:15;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 5;">
                <img src="/frontend-assets/assets/img/OBJ_triangle_s.png" alt="Image description" data-ww="['55px','55px','55px','56px']" data-hh="48px" width="111" height="96" data-no-retina>
              </div>

              <!-- LAYER NR. 2 -->
              <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
                   id="slide-967-layer-7"
                   data-x="['left','left','left','left']" data-hoffset="['-146','-146','-50','-86']"
                   data-y="['top','top','top','top']" data-voffset="['38','38','131','54']"
                   data-width="none"
                   data-height="none"
                   data-whitespace="nowrap"

                   data-type="image"
                   data-responsive_offset="on"

                   data-frames='[{"delay":200,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:240deg;opacity:0;fb:20px;","to":"o:1;rZ:-25;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 6;">
                <img src="/frontend-assets/assets/img/OBJ_pen.png" alt="Image description" data-ww="['530px','530px','300px','265px']" data-hh="['60px','60px','34px','30px']" width="530" height="60" data-no-retina>
              </div>

              <!-- LAYER NR. 3 -->
              <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme  tp-joyshadow rs-parallaxlevel-15"
                   id="slide-967-layer-15"
                   data-x="['center','center','center','center']" data-hoffset="['260','260','260','260']"
                   data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                   data-width="2500"
                   data-height="['500','500','400','300']"
                   data-whitespace="nowrap"

                   data-type="shape"
                   data-responsive_offset="on"

                   data-frames='[{"delay":100,"speed":750,"frame":"0","from":"sX:3;sY:3;opacity:0;","to":"o:1;rZ:45;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 7;background:linear-gradient(140deg, rgba(136,136,212,1) 0%, rgba(82,82,212,1) 100%);"></div>

              <!-- LAYER NR. 4 -->
              <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-10"
                   id="slide-967-layer-20"
                   data-x="['left','left','left','left']" data-hoffset="['1079','1079','587','315']"
                   data-y="['top','top','top','top']" data-voffset="['512','512','692','471']"
                   data-width="['100','100','100','50']"
                   data-height="['100','100','100','50']"
                   data-whitespace="nowrap"

                   data-type="shape"
                   data-responsive_offset="on"

                   data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:360deg;opacity:0;","to":"o:1;rZ:35;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 8;border-color:rgb(255,255,255);border-style:solid;border-width:3px 3px 3px 3px;">
                <div class="rs-looped rs-wave" data-speed="8" data-angle="0" data-radius="3px" data-origin="50% 50%"></div>
              </div>

              <!-- LAYER NR. 5 -->
              <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                   id="slide-967-layer-1"
                   data-x="['center','center','center','center']" data-hoffset="['0','0','0','-1']"
                   data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','-3']"
                   data-width="2500"
                   data-height="['500','500','400','300']"
                   data-whitespace="nowrap"

                   data-type="shape"
                   data-basealign="slide"
                   data-responsive_offset="on"

                   data-frames='[{"delay":200,"speed":750,"frame":"0","from":"sX:3;sY:3;opacity:0;","to":"o:1;rZ:-15;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"
             
              
                   style="z-index: 9;background-color:{{$color}};"
                   ></div>

              <!-- LAYER NR. 6 -->
              <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
                   id="slide-967-layer-11"
                   data-x="['left','left','left','left']" data-hoffset="['76','76','58','-23']"
                   data-y="['top','top','top','top']" data-voffset="['593','593','677','503']"
                   data-width="none"
                   data-height="none"
                   data-whitespace="nowrap"

                   data-type="image"
                   data-responsive_offset="on"

                   data-frames='[{"delay":200,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:-180deg;opacity:0;fb:20px;","to":"o:1;rZ:25;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 10;">
                <img src="/frontend-assets/assets/img/OBJ_mouse.png" alt="Image description" data-ww="['300px','300px','150px','150px']" data-hh="['400px','400px','200','200px']" width="300" height="400" data-no-retina>
              </div>

              <!-- LAYER NR. 7 -->
              {{-- <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
                   id="slide-967-layer-13"
                   data-x="['left','left','left','left']" data-hoffset="['884','884','579','355']"
                   data-y="['top','top','top','top']" data-voffset="['232','232','288','204']"
                   data-width="none"
                   data-height="none"
                   data-whitespace="nowrap"

                   data-type="image"
                   data-responsive_offset="on"

                   data-frames='[{"delay":200,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:-180deg;opacity:0;fb:20px;","to":"o:1;rZ:15;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 11;">
                <img src="/frontend-assets/assets/img/OBJ_eraser.png" alt="Image description" data-ww="['230px','230px','20%','165px']" data-hh="['130px','130px','130px','93px']" width="230" height="130" data-no-retina>
              </div> --}}

              <!-- LAYER NR. 8 -->
              <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-2"
                   id="slide-967-layer-6"
                   data-x="['right','right','center','center']" data-hoffset="['100','0','0','0']"
                   data-y="['middle','middle','middle','middle']" data-voffset="['140','0','0','0']"
                   data-width="['500','600','460','300']"
                   data-height="['150',150','100','100']"
                   data-whitespace="nowrap"

                   data-type="shape"
                   data-responsive_offset="on"

                   data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.7;sY:0.7;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 12;border-color:rgb(255,255,255);border-style:solid;border-width:5px 5px 5px 5px;background:#00000017">
                  <div class="">
                    <div class="container">
                      <div style="display:flex;flex-direction:row">
                        <div style="flex:9; margin-top:1rem;">
                          <input type="text" placeholder="Search Your Area" class="search"/>
                        </div>
                        <div style="flex:2;margin-top:2rem;">
                          <span class="key_icon">
                          <i class="fa fa-location-arrow"></i>
                          </span>
                          </div>
                      </div>
                      <p class="search_text">Login For Your Recent Search</p>
                    </div>
                  </div>
                  </div>

              <!-- LAYER NR. 9 -->
              <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
                   id="slide-967-layer-2"
                   data-x="['center','center','center','center']" data-hoffset="['250','0','0','0']"
                   data-y="['middle','middle','middle','middle']" data-voffset="['-120','-120','-100','-80']"
                   data-fontsize="['40','50','80','50']"
                   data-lineheight="['100','100','80','50']"
                   data-letterspacing="['0','0','0','0']"
                   data-width="none"
                   data-height="none"
                   data-whitespace="nowrap"

                   data-type="text"
                   data-responsive_offset="on"

                   {{-- data-frames='[{"delay":550,"split":"chars","splitdelay":0.1000000000000000055511151231257827021181583404541015625,"speed":2500,"split_direction":"random","frame":"0","from":"y:50px;sX:1;sY:1;opacity:0;fb:20px;","color":"#001536","to":"o:1;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' --}}
                  data-frames='[{"delay":"+290","split":"chars","splitdelay":0.05000000000000000277555756156289135105907917022705078125,"speed":2000,"split_direction":"forward","frame":"0","from":"opacity:0;","color":"#000000","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","color":"transparent","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[30,30,30,20]"

                   style="z-index: 13; white-space: nowrap; font-size: 50px; line-height: 100px; font-weight: 500; color: #ffffff; letter-spacing: 30px;font-family:Poppins;">WHY NOT RESERVE TABLE
              </div>

              <!-- LAYER NR. 10 -->
              <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
                   id="slide-967-layer-3"
                   data-x="['center','center','center','center']" data-hoffset="['350','0','0','0']"
                   data-y="['middle','middle','middle','middle']" data-voffset="['-60','0','0','0']"
                   data-fontsize="['40','50','80','50']"
                   data-lineheight="['100','100','80','50']"
                   data-letterspacing="['0','0','0','0']"
                   data-width="none"
                   data-height="none"
                   data-whitespace="nowrap"

                   data-type="text"
                   data-responsive_offset="on"

                   {{-- data-frames='[{"delay":600,"split":"chars","splitdelay":0.1000000000000000055511151231257827021181583404541015625,"speed":2500,"split_direction":"random","frame":"0","from":"y:50px;sX:1;sY:1;opacity:0;fb:20px;","color":"#001536","to":"o:1;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' --}}
                  data-frames='[{"delay":"+290","split":"chars","splitdelay":0.05000000000000000277555756156289135105907917022705078125,"speed":2000,"split_direction":"forward","frame":"0","from":"opacity:0;","color":"#000000","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","color":"transparent","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                   
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[30,30,30,20]"

                   style="z-index: 14; white-space: nowrap; font-size: 100px; line-height: 100px; font-weight: 500; color: #ffffff; letter-spacing: 30px;font-family:Poppins;">AT YOUR FAVOURITE RESTAURANT?
              </div>

              <!-- LAYER NR. 11 -->
              {{-- <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
                   id="slide-967-layer-4"
                   data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                   data-y="['middle','middle','middle','middle']" data-voffset="['120','120','100','80']"
                   data-fontsize="['100','100','80','50']"
                   data-lineheight="['100','100','80','50']"
                   data-letterspacing="['30','30','30','20']"
                   data-width="none"
                   data-height="none"
                   data-whitespace="nowrap"

                   data-type="text"
                   data-responsive_offset="on"

                   data-frames='[{"delay":650,"split":"chars","splitdelay":0.1000000000000000055511151231257827021181583404541015625,"speed":2500,"split_direction":"random","frame":"0","from":"y:50px;opacity:0;fb:20px;","color":"#001536","to":"o:1;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[30,30,30,20]"

                   style="z-index: 15; white-space: nowrap; font-size: 100px; line-height: 100px; font-weight: 600; color: #ffffff; letter-spacing: 30px;font-family:Poppins;">JOY
              </div> --}}

              <!-- LAYER NR. 12 -->
              <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-10"
                   id="slide-967-layer-27"
                   data-x="['left','left','left','left']" data-hoffset="['1047','1047','715','715']"
                   data-y="['top','top','top','top']" data-voffset="['728','728','574','574']"
                   data-width="50"
                   data-height="100"
                   data-whitespace="nowrap"

                   data-type="shape"
                   data-responsive_offset="on"

                   data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:360deg;opacity:0;","to":"o:1;rZ:15;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 16;border-color:rgb(255,255,255);border-style:solid;border-width:3px 3px 3px 3px;border-radius:25px 25px 25px 25px;">
                <div class="rs-looped rs-wave" data-speed="5" data-angle="0" data-radius="4px" data-origin="50% 50%"></div>
              </div>

              <!-- LAYER NR. 13 -->
              {{-- <div class="tp-caption   tp-resizeme rs-parallaxlevel-4"
                   id="slide-967-layer-12"
                   data-x="['left','left','left','left']" data-hoffset="['687','687','452','271']"
                   data-y="['top','top','top','top']" data-voffset="['356','356','499','328']"
                   data-width="none"
                   data-height="none"
                   data-whitespace="nowrap"

                   data-type="image"
                   data-responsive_offset="on"

                   data-frames='[{"delay":200,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:-140deg;opacity:0;fb:20px;","to":"o:1;rZ:-15;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 17;">
                <img src="/frontend-assets/assets/img/OBJ_headphones.png" alt="Image description" data-ww="['710px','710px','400px','355px']" data-hh="['630px','630px','355px','315px']" width="710" height="630" data-no-retina>
              </div> --}}

              <!-- LAYER NR. 14 -->
              <div class="tp-caption   tp-resizeme rs-parallaxlevel-3"
                   id="slide-967-layer-8"
                   data-x="['left','left','left','left']" data-hoffset="['332','332','175','82']"
                   data-y="['top','top','top','top']" data-voffset="['-3','-3','109','54']"
                   data-width="none"
                   data-height="none"
                   data-whitespace="nowrap"

                   data-type="image"
                   data-responsive_offset="on"

                   data-frames='[{"delay":200,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:-180deg;opacity:0;fb:20px;","to":"o:1;rZ:-15;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 18;">
                <img src="/frontend-assets/assets/img/food2.png" alt="Image description" data-ww="['500px','500px','350px','250px']" data-hh="['170px','170px','119px','85px']" width="500" height="170" data-no-retina>
              </div>

              <!-- LAYER NR. 15 -->
              <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-10"
                   id="slide-967-layer-21"
                   data-x="['left','left','left','left']" data-hoffset="['1158','1158','658','416']"
                   data-y="['top','top','top','top']" data-voffset="['19','19','334','60']"
                   data-width="['100','100','50','50']"
                   data-height="['100','100','50','50']"
                   data-whitespace="nowrap"

                   data-type="shape"
                   data-responsive_offset="on"

                   data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 19;border-color:rgb(255,255,255);border-style:solid;border-width:3px 3px 3px 3px;border-radius:50px 50px 50px 50px;">
                <div class="rs-looped rs-wave" data-speed="3" data-angle="0" data-radius="4px" data-origin="50% 50%"></div>
              </div>

              <!-- LAYER NR. 16 -->
              <div class="tp-caption   tp-resizeme rs-parallaxlevel-3"
                   id="slide-967-layer-9"
                   data-x="['left','left','left','left']" data-hoffset="['899','899','490','306']"
                   data-y="['top','top','top','top']" data-voffset="['-136','-136','31','24']"
                   data-width="none"
                   data-height="none"
                   data-whitespace="nowrap"

                   data-type="image"
                   data-responsive_offset="on"

                   data-frames='[{"delay":200,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:-180deg;opacity:0;fb:20px;","to":"o:1;rZ:-15;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 20;">
                <img src="/frontend-assets/assets/img/OBJ_watch.png" alt="Image description" data-ww="['320px','320px','200px','160px']" data-hh="['450px','450px','281px','225px']" width="320" height="450" data-no-retina>
              </div>

              <!-- LAYER NR. 17 -->
              <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-10"
                   id="slide-967-layer-22"
                   data-x="['left','left','left','left']" data-hoffset="['-203','-203','-4','42']"
                   data-y="['top','top','top','top']" data-voffset="['349','349','328','360']"
                   data-width="['10','10','10','5']"
                   data-height="['10','10','10','5']"
                   data-whitespace="nowrap"

                   data-type="shape"
                   data-responsive_offset="on"

                   data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 21;">
                <div class="rs-looped rs-wave" data-speed="6" data-angle="0" data-radius="6px" data-origin="50% 50%">
                  
                </div>
                <img src="/frontend-assets/assets/img/food3.png" width="5" height="5" data-no-retina>

              </div>

              <!-- LAYER NR. 18 -->
              {{-- <div class="tp-caption   tp-resizeme rs-parallaxlevel-3"
                   id="slide-967-layer-10"
                   data-x="['left','left','left','left']" data-hoffset="['-94','-294','-82','-106']"
                   data-y="['top','top','top','top']" data-voffset="['179','179','285','158']"
                   data-width="none"
                   data-height="none"
                   data-whitespace="nowrap"

                   data-type="image"
                   data-responsive_offset="on"

                   data-frames='[{"delay":200,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:180deg;sX:1;sY:1;opacity:0;fb:20px;","to":"o:1;rZ:15;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 22;">
                <img src="/frontend-assets/assets/img/table.png" alt="Image description" data-ww="['390px','390px','250px','195px']" data-hh="['620px','620px','397px','310px']" width="390" height="620" data-no-retina>
              </div> --}}

          

              <!-- LAYER NR. 20 -->
              <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                   id="slide-967-layer-16"
                   data-x="['left','left','center','left']" data-hoffset="['100','100','0','-226']"
                   data-y="['bottom','bottom','bottom','bottom']" data-voffset="['50','50','30','64']"
                   data-width="200"
                   data-height="5"
                   data-whitespace="nowrap"
                   data-visibility="['on','on','on','off']"

                   data-type="shape"
                   data-basealign="slide"
                   data-responsive_offset="on"

                   data-frames='[{"delay":2500,"speed":1000,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:[-100%];y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 24;background-color:rgb(255,255,255);"></div>

              <!-- LAYER NR. 21 -->
              <div class="tp-caption   tp-resizeme tp-svg-layer"
                   id="slide-967-layer-17"
                   data-x="['left','left','center','left']" data-hoffset="['175','175','0','-153']"
                   data-y="['bottom','bottom','bottom','bottom']" data-voffset="['86','86','50','91']"
                   data-width="50"
                   data-height="50"
                   data-whitespace="nowrap"
                   data-visibility="['on','on','on','off']"

                   data-type="svg"
                   data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":"","speed":"1500","ease":"Power4.easeOut"}]'
                   data-svg_src="/frontend-assets/assets/img/ic_arrow_downward_24px.svg"
                   data-svg_idle="sc:transparent;sw:0;sda:0;sdo:0;"
                   data-basealign="slide"
                   data-responsive_offset="on"

                   data-frames='[{"delay":2500,"speed":1000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 25; min-width: 50px; max-width: 50px; max-width: 50px; max-width: 50px; color: #ffffff;cursor:pointer;">
                <div class="rs-looped rs-slideloop" data-easing="Power1.easeIn" data-speed="1" data-xs="0" data-xe="0" data-ys="-10" data-ye="10"></div>
              </div>

              <!-- LAYER NR. 22 -->
              <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-12"
                   id="slide-967-layer-19"
                   data-x="['left','left','left','left']" data-hoffset="['94','94','123','39']"
                   data-y="['top','top','top','top']" data-voffset="['77','77','138','65']"
                   data-width="['60','60','60','30']"
                   data-height="['60','60','60','30']"
                   data-whitespace="nowrap"

                   data-type="shape"
                   data-responsive_offset="on"

                   data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 26;border-color:rgb(255,255,255);border-style:solid;border-width:3px 3px 3px 3px;border-radius:50px 50px 50px 50px;">
                <div class="rs-looped rs-wave" data-speed="6" data-angle="0" data-radius="5px" data-origin="50% 50%"></div>
              </div>

              <!-- LAYER NR. 23 -->
              <div class="tp-caption   tp-resizeme rs-parallaxlevel-12"
                   id="slide-967-layer-23"
                   data-x="['left','left','left','left']" data-hoffset="['1081','1081','657','394']"
                   data-y="['top','top','top','top']" data-voffset="['109','109','128','298']"
                   data-width="none"
                   data-height="none"
                   data-whitespace="nowrap"

                   data-type="image"
                   data-responsive_offset="on"

                   data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:360deg;opacity:0;","to":"o:1;rZ:25;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 27;">
                <div class="rs-looped rs-wave" data-speed="7" data-angle="0" data-radius="5px" data-origin="50% 50%">
                  <img src="/frontend-assets/assets/img/OBJ_triangle.png" alt="Image description" data-ww="['110px','110px','110px','110px']" data-hh="['95px','95px','95px','95px']" width="221" height="191" data-no-retina>
                </div>
              </div>

              <!-- LAYER NR. 24 -->
              <div class="tp-caption   tp-resizeme rs-parallaxlevel-12"
                   id="slide-967-layer-24"
                   data-x="['left','left','left','left']" data-hoffset="['-31','-31','31','-34']"
                   data-y="['top','top','top','top']" data-voffset="['548','548','589','494']"
                   data-width="none"
                   data-height="none"
                   data-whitespace="nowrap"

                   data-type="image"
                   data-responsive_offset="on"

                   data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:360deg;opacity:0;","to":"o:1;rZ:-40;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 28;">
                <div class="rs-looped rs-wave" data-speed="6" data-angle="0" data-radius="6px" data-origin="50% 50%">
                  <img src="/frontend-assets/assets/img/OBJ_triangle.png" alt="Image description" data-ww="['110px','110px','110px','110px']" data-hh="['95px','95px','95px','95px']" width="221" height="191" data-no-retina>
                </div>
              </div>

              <!-- LAYER NR. 25 -->
              <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-12"
                   id="slide-967-layer-26"
                   data-x="['left','left','left','left']" data-hoffset="['42','42','24','57']"
                   data-y="['top','top','top','top']" data-voffset="['220','257','209','226']"
                   data-width="['40','40','40','20']"
                   data-height="['40','40','40','20']"
                   data-whitespace="nowrap"

                   data-type="shape"
                   data-responsive_offset="on"

                   data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:360deg;opacity:0;","to":"o:1;rZ:45;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 29;">
                <div class="rs-looped rs-wave" data-speed="5" data-angle="0" data-radius="5px" data-origin="50% 50%"></div>
                <img src="/frontend-assets/assets/img/fork.png" alt="Image description" data-ww="['110px','110px','110px','110px']" data-hh="['95px','95px','95px','95px']" width="221" height="191" data-no-retina>

              </div>

              <!-- LAYER NR. 26 -->
              <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-11"
                   id="slide-967-layer-28"
                   data-x="['left','left','left','left']" data-hoffset="['1173','1173','764','764']"
                   data-y="['top','top','top','top']" data-voffset="['652','652','723','723']"
                   data-width="30"
                   data-height="30"
                   data-whitespace="nowrap"

                   data-type="shape"
                   data-responsive_offset="on"

                   data-frames='[{"delay":700,"speed":1500,"frame":"0","from":"x:center;y:middle;rZ:360deg;opacity:0;","to":"o:1;rZ:-15;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                   data-textAlign="['inherit','inherit','inherit','inherit']"
                   data-paddingtop="[0,0,0,0]"
                   data-paddingright="[0,0,0,0]"
                   data-paddingbottom="[0,0,0,0]"
                   data-paddingleft="[0,0,0,0]"

                   style="z-index: 30;border-color:rgb(255,255,255);border-style:solid;border-width:3px 3px 3px 3px;">
                <div class="rs-looped rs-wave" data-speed="8" data-angle="0" data-radius="8px" data-origin="50% 50%"></div>
              </div>
            </li>
          </ul>
          <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
      </div><!-- END REVOLUTION SLIDER -->
      {{-- {{$colors->shuffle()}} --}}

      <section class="g-py-40">
        <div style="margin:0 8%;">
          <h4 class="landing_title">What's on the menu?<h4> 
          <div class="row">
              <div class="col-lg-6 g-mb-30">
                <!-- Article -->
                <article class="u-block-hover">
                  <figure class="u-bg-overlay g-bg-black-gradient-opacity-v1--after">
                    <img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="/img/banner1.jpg" alt="Image Description">
                  </figure>
            
            
                  <div class="g-pos-abs" style="bottom:33%; width:100%">
                
                    <div class="text-center">
                    <h3 class="h4 g-font-weight-600 g-mt-5" style="font-size: 30px;">
                      <a class="g-color-white g-color-white--hover" href="#">Office Catering</a>
                    </h3>
                  </div>
                  </div>
                </article>
                <p class="landing_description"> Nullam elementum tincidunt massa, a pulvinar leo ultricies ut. Ut fringilla lectus tellusimp imperdiet molestie est volutpat at. Nullam elementum tincidunt massa, a pulvinar leo ultricies ut. Ut fringilla lectus tellusimp imperdiet molestie est volutpat at. Sed viverra cursus nibh.</p>
                <a class="landing_a"  href="#">Read More.</a>
                
                <!-- End Article -->
              </div>
            
              <div class="col-lg-6 g-mb-30">
                <!-- Article -->
                <article class="u-block-hover">
                  <figure class="u-bg-overlay g-bg-black-gradient-opacity-v1--after">
                    <img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="/img/banner1.jpg" alt="Image Description">
                  </figure>
            
            
                  <div class="g-pos-abs" style="bottom:33%; width:100%">
                  
                    <div class="text-center">
                      <h3 class="h4 g-font-weight-600 g-mt-5" style="font-size: 30px;">
                        <a class="g-color-white g-color-white--hover" href="#">Comfort Food</a>
                      </h3>
                    </div>
                  </div>
                </article>
                <p class="landing_description">Nullam elementum tincidunt massa, a pulvinar leo ultricies ut. Ut fringilla lectus tellusimp imperdiet molestie est volutpat at. Sed viverra cursus nibh.Nullam elementum tincidunt massa, a pulvinar leo ultricies ut. Ut fringilla lectus tellusimp imperdiet molestie est volutpat at.</p>
                <a class="landing_a"  href="#">Read More.</a>
                
                <!-- End Article -->
              </div>
            
          </div>
        </div>
      </section>
      <section class=" g-brd-gray-light-v4">
          <div style="margin:0 0%;">
            <div class="row diagonal-split-background " style="height:12rem;">
              <div class="col-lg-3 align-self-lg-center">
                <p class="partner_left_text">Own a</p>
                <p class="partner_left_text">Restaurant?</p>
                {{-- <p class="partner_left_text">One Corner</p> --}}
               </div>
        
              <div class="col-lg-7 align-self-lg-center text-center">
                <p class="partner_right_text">Some Text Here. Some Text Here. Some Text Here.</p>
                <a class="btn u-btn-primary rounded-0 g-py-12 g-px-30" style="font-size:18px" href="/partnership">Partner With Us</a>
              </div>
            </div>
          </div>
        </section>
@endsection
    

   
@section('js')
  <!-- JS Revolution Slider -->
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>

  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution-addons/polyfold/js/revolution.addon.polyfold.min.js"></script>

  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>

  <!-- JS Plugins Init. -->
  <script>

    $(document).on('ready', function () {
      // initialization of carousel
      $.HSCore.components.HSCarousel.init('.js-carousel');

      // initialization of masonry
      $('.masonry-grid').imagesLoaded().then(function () {
        $('.masonry-grid').masonry({
          columnWidth: '.masonry-grid-sizer',
          itemSelector: '.masonry-grid-item',
          percentPosition: true
        });
      });

      // Header
      $.HSCore.components.HSHeader.init($('#js-header'));
      $.HSCore.helpers.HSHamburgers.init('.hamburger');

      // Initialization of HSMegaMenu plugin
      $('.js-mega-menu').HSMegaMenu({
        event: 'hover',
        pageContainer: $('.container'),
        breakpoint: 991
      });
    });

    var revapi347,
      tpj = jQuery;

    tpj(document).ready(function () {
      if (tpj("#rev_slider_347_1").revolution == undefined) {
        revslider_showDoubleJqueryError("#rev_slider_347_1");
      } else {
        revapi347 = tpj("#rev_slider_347_1").show().revolution({
          sliderType: "hero",
          jsFileLocation: "//server.local/revslider/wp-content/plugins/revslider/public/assets/js/",
          // sliderLayout: "fullscreen",
          dottedOverlay: "none",
          delay: 9000,
          responsiveLevels: [1240, 1240, 778, 480],
          visibilityLevels: [1240, 1240, 778, 480],
          gridwidth: [1240, 1240, 778, 480],
          gridheight: [560, 560, 960, 720],
          lazyType: "none",
          parallax: {
            type: "scroll",
            origo: "slidercenter",
            speed: 1000,
            speedbg: 0,
            speedls: 2000,
            levels: [8, 16, 24, 32, -8, -16, -24, -32, 36, 2, 4, 6, 50, -30, -20, 55],
          },
          shadow: 0,
          spinner: "spinner1",
          autoHeight: "off",
          fullScreenAutoWidth: "off",
          fullScreenAlignForce: "off",
          fullScreenOffsetContainer: "",
          fullScreenOffset: "",
          disableProgressBar: "on",
          hideThumbsOnMobile: "off",
          hideSliderAtLimit: 0,
          hideCaptionAtLimit: 0,
          hideAllCaptionAtLilmit: 0,
          debugMode: false,
          fallbacks: {
            simplifyAll: "off",
            disableFocusListener: false,
          }
        });
      }
      try {
        initSocialSharing("347")
      } catch (e) {
      }
    });
  </script>

  @endsection