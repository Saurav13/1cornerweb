@extends('layouts.app')


@section('css')
<style>
    .page_title{
        font-size:36px;
    }
    .top_pull{
            margin-top:-10rem;
        }
        .clipped_right{
            clip-path: polygon(0% 0%, 70% 0, 100% 50%, 70% 100%, 0% 100%);
        }
        .clipped_left{
            clip-path: polygon(30% 0%, 100% 0%, 100% 100%, 30% 100%, 0% 50%);
        }
        .restaurant_name{
            font-size:26px;
        }
        .restaurant_pax{
            font-size: 18px;
            color:black;
        }
        .order_item_side_img{
            background-image: url(/img/order.jpg);
            height:100%;
        }
       .sp_request{
            line-height:18px;
            text-align: justify;
        }
</style>
@endsection

@section('body')
<section style="background:#efefef" class="g-pb-40">
        <div class="row align-items-stretch">
            <div class="col-lg-12 g-mb-30">
                <!-- Article -->
                <article class="text-center g-color-white g-overflow-hidden">
                    <div class="g-min-height-300 g-flex-middle g-bg-cover g-bg-size-cover g-bg-bluegray-opacity-0_3--after g-transition-0_5" data-bg-img-src="/img/banner4.jpg">
                        <div class="g-flex-middle-item g-pos-rel g-z-index-1 g-pb-50 g-pt-10 g-px-20">
                            <h3 class="page_title">Reservation History</h3>
                        </div>
                    </div>
                </article>
                <!-- End Article -->
            </div>
            
            
        </div>


        <div class="row justify-content-center">
            <div class="col-sm-10 col-md-9 col-lg-6 top_pull">
                <div class="u-shadow-v21 g-bg-white rounded g-py-20 g-px-30">

                    <div class="row">
                        <div class="col-lg-12 g-mb-30">
                            <!-- Event Listing -->
                            <article class="u-shadow-v39">
                            <div class="row">
                                <div class="col-5 g-px-10">
                                <div class="g-bg-img-hero order_item_side_img clipped_right" style=""></div>
                                </div>
                        
                                <div class="col-7 g-px-10">
                                <div class="media g-py-20">
                                    <div class="g-pr-20">
                                        <h3 class="g-line-height-1 g-color-black mb-1 restaurant_name">Delhi O Delhi</h3>
                                        <span class="restaurant_pax mb-1"><i class="fas fa-user"></i>&nbsp; 8 People</span><br>
                                        <span class="restaurant_pax"><i class="fas fa-calendar"></i>&nbsp; June 15</span> &nbsp;&nbsp;&nbsp;
                                        <span class="restaurant_pax"><i class="fas fa-clock"></i>&nbsp; 7:30 AM</span><br>
                                         <p class="restaurant_pax sp_request"># Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                         </p>
                                    </div>
                                </div>
                            </div>
                            </article>
                            <!-- End Event Listing -->
                        </div>
                        <div class="col-lg-12 g-mb-30">
                            <!-- Event Listing -->
                            <article class="u-shadow-v39 g-pl-30">
                            <div class="row">

                                <div class="col-7  g-px-10">
                                    <div class="media  g-py-20">
                                        <div class="">
                                            <h3 class="g-line-height-1 g-color-black mb-1 restaurant_name">Delhi O Delhi</h3>
                                            <span class="restaurant_pax mb-1"><i class="fas fa-user"></i>&nbsp; 8 People</span><br>
                                            <span class="restaurant_pax"><i class="fas fa-calendar"></i>&nbsp; June 15</span> &nbsp;&nbsp;&nbsp;
                                            <span class="restaurant_pax"><i class="fas fa-clock"></i>&nbsp; 7:30 AM</span>
                                            

                                        {{-- <h3 class="g-line-height-1 mb-0"><a class="u-link-v5 g-color-black g-color-primary--hover g-font-size-18" href="#">Camino de Santiago: treading the ancient path to the end of the earth</a></h3> --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-5  g-px-10">
                                <div class="g-bg-img-hero order_item_side_img clipped_left" style=""></div>
                                </div>
                        
                            </div>
                            </article>
                            <!-- End Event Listing -->
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
</section>

@endsection