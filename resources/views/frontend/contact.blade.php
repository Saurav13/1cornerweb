@extends('layouts.app')


@section('css')
    <link   rel="stylesheet" href="/css/materialInputs.css"/>
    <style>
        .top_pull{
            margin-top:-10rem;
        }

       
        .page_title{
            font-size:36px;
        }
    </style>
@endsection


@section('body')
<section style="background:#efefef" class="g-pb-40">
    <div class="row align-items-stretch">
        <div class="col-lg-12 g-mb-30">
            <!-- Article -->
            <article class="text-center g-color-white g-overflow-hidden">
                <div class="g-min-height-300 g-flex-middle g-bg-cover g-bg-size-cover g-bg-bluegray-opacity-0_3--after g-transition-0_5" data-bg-img-src="/img/banner4.jpg">
                    <div class="g-flex-middle-item g-pos-rel g-z-index-1 g-pb-50 g-pt-10 g-px-20">
                        <h3 class="page_title">Contact Us</h3>
                    </div>
                </div>
            </article>
            <!-- End Article -->
        </div>
        
        
    </div>


    <div class="row justify-content-center">
        <div class="col-sm-10 col-md-9 col-lg-6 top_pull">
        <div class="u-shadow-v21 g-bg-white rounded g-py-20 g-px-30">
           

            <!-- Form -->
            <form class="g-py-15">
           

            <header class="mb-3">
                    <h2 class="h2 g-color-black ">Your Details</h2>
            </header>
            <hr style="margin:0px">            
            <div class="row">
                <div class="col-xs-12 col-sm-6 mb-4">
                        <label class="pure-material-textfield-outlined"  >
                                <!-- <i class="fa fa-users pull-right" ></i> -->
                            <input type="text" placeholder=" ">
                                
                            <span>First Name </span>
                        </label>
                </div>
                <div class="col-xs-12 col-sm-6 mb-4">
                        <label class="pure-material-textfield-outlined"  >
                                <!-- <i class="fa fa-users pull-right" ></i> -->
                            <input type="text" placeholder=" ">
                                
                            <span>Last Name </span>
                        </label>
                </div>
                <div class="col-xs-12 col-sm-6 mb-4">
                        <label class="pure-material-textfield-outlined"  >
                                <!-- <i class="fa fa-users pull-right" ></i> -->
                            <input type="email" placeholder=" ">
                                
                            <span>Email </span>
                        </label>
                </div>
                <div class="col-xs-12 col-sm-6 mb-4">
                        <label class="pure-material-textfield-outlined"  >
                                <!-- <i class="fa fa-users pull-right" ></i> -->
                            <input type="number" placeholder=" ">
                                
                            <span>Phone Number </span>
                        </label>
                </div>
                <div class="col-xs-12 col-sm-12 mb-4">
                        <label class="pure-material-textfield-outlined"  >
                                <!-- <i class="fa fa-users pull-right" ></i> -->
                                <textarea  style="height:7rem" placeholder=" "></textarea>
                            <span>Message</span>
                        </label>
                </div>
            </div>

            <div class="row justify-content-between mb-5">
                <div class="col-8 align-self-center">
               
                </div>
                <div class="col-4 align-self-center text-right">
                <button class="btn btn-md u-btn-primary rounded " type="button">Submit</button>
                </div>
            </div>
            </form>
            <!-- End Form -->

        </div>
        </div>
    </div>

</section>


@endsection