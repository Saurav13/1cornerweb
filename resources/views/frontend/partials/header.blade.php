<!DOCTYPE html>
<html lang="en" class="font-primary">
  <head>
    <title>One Corners | Website</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Fav-->
    <link rel="shortcut icon" href="/frontend-assets/main-assets/favicon.ico">

    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto%3A700%2C300">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Poppins:600">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/bootstrap/offcanvas.css">

    <!-- CSS Implementing Plugins -->
    <link  rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/jquery-ui/themes/base/jquery-ui.min.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-etlinefont/style.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-hs/style.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-line-pro/style.css">

    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/animate.css">
    
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/hamburgers/hamburgers.min.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.min.css">

  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/fancybox/jquery.fancybox.css">

    <!-- CSS Unify -->
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify.css">

    <!-- Revolution Slider -->
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/css/settings.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution-addons/polyfold/css/revolution.addon.polyfold.css">
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,500,700|Roboto+Slab:400,700|Material+Icons">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@yield('css')

    <style>
       * {
          font-family: Roboto Condensed;
      }
      button, input, optgroup, select, textarea{
        font-family: Roboto Condensed;
        
      }
    </style>
  </head>
