
<style>
  .menu_item{
    font-size: 22px;
    font-weight:500
  }

  .menu_menu{
    font-size: 36px;
    font-weight: 300;
  }
  .toggler_main .toggle_color{
    border-color:#486167;
    color:#486167;
  }
  
  .un_toggle_color{
    border-color:white;
    color:white;
  }
  .un_toggle_color:hover{
    border-color:white;
    color:white;
    background:#48616780; 
  }
  </style>
<body class="u-body--header-side-overlay-right">
  <main>
    <!-- Header -->
    {{-- <header class="u-header u-header--static">
      <div class="u-header__section u-header__section--light g-bg-white g-transition-0_3 g-py-10">
        <nav class="js-mega-menu navbar navbar-expand-lg">
          <div class="container">
            <!-- Responsive Toggle Button -->
            <button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-3 g-right-0" type="button"
                    aria-label="Toggle navigation"
                    aria-expanded="false"
                    aria-controls="navBar"
                    data-toggle="collapse"
                    data-target="#navBar">
              <span class="hamburger hamburger--slider">
                <span class="hamburger-box">
                  <span class="hamburger-inner"></span>
                </span>
              </span>
            </button>
            <!-- End Responsive Toggle Button -->

            <!-- Logo -->
            <a href="/frontend-assets/main-assets/index.html" class="navbar-brand">
              <img src="/frontend-assets/main-assets/assets/img/logo/logo-1.png" style="height:3rem" alt="Image Description">
            </a>
            <!-- End Logo -->

          

            <div class="d-inline-block g-hidden-xs-down g-pos-rel g-valign-middle g-pl-30 g-pl-0--lg">
              <a class="btn u-btn-outline-primary g-font-size-13 text-uppercase g-py-10 g-px-15" href="https://wrapbootstrap.com/theme/unify-responsive-website-template-WB0412697?ref=htmlstream" target="_blank">Login</a>
              <a class="btn u-btn-outline-primary g-font-size-13 text-uppercase g-py-10 g-px-15 " id="header-toggler" aria-haspopup="true" aria-expanded="false" aria-controls="js-header" aria-label="Toggle Header" data-target="#js-header" href="https://wrapbootstrap.com/theme/unify-responsive-website-template-WB0412697?ref=htmlstream" target="_blank">Menu</a>
            
            </div>
          </div>
        </nav>
      </div>

    </header> --}}


    <header id="js-header-top" class="u-header u-header--abs-top u-header--show-hide u-header--change-logo u-header--change-appearance" data-header-fix-moment="500" data-header-fix-effect="slide">
      <button hidden class="btn u-btn-white u-header-toggler u-header-toggler--top-left g-pa-0" id="header-toggler" aria-haspopup="true" aria-expanded="false" aria-controls="js-header" aria-label="Toggle Header" data-target="#js-header">
        <span class="hamburger hamburger--collapse">
      <span class="hamburger-box">
        <span class="hamburger-inner"></span>
        </span>
        </span>
      </button>
      <div class="u-header__section u-header__section--dark g-bg-black-opacity-0_1 g-py-10 g-px-100" data-header-fix-moment-exclude="u-header__section--dark g-bg-black-opacity-0_1 g-py-10" data-header-fix-moment-classes="u-header__section--light g-bg-white u-shadow-v18 g-py-11 toggler_main">
        <a href="/" class="navbar-brand">
          <img src="/frontend-assets/main-assets/assets/img/logo/logo-1.png" style="height:3rem" alt="Image Description">
        </a>
       
        <div class="d-inline-block g-hidden-xs-down g-pos-rel g-valign-middle g-pl-30 g-pl-0--lg" style="float:right">
            <a class="btn u-btn-outline-primary g-font-size-13 text-uppercase g-py-10 g-px-15 un_toggle_color toggle_color" href="/login" ><i class="fa fa-user"></i>&nbsp; Login</a>
            <a class="btn u-btn-outline-primary g-font-size-13 text-uppercase g-py-10 g-px-15 un_toggle_color toggle_color" href="#" id="menuToggler" ><i class="fa fa-bars"></i>&nbsp; Menu</a>
            
          </div>
      </div>

    </header>
   
    <!-- End Header -->

{{-- 
    <header class="u-header u-header--abs-top u-header--show-hide u-header--change-logo u-header--change-appearance">
        <button hidden class="btn u-btn-white u-header-toggler u-header-toggler--top-left g-pa-0" id="header-toggler" aria-haspopup="true" aria-expanded="false" aria-controls="js-header" aria-label="Toggle Header" data-target="#js-header">
            <span class="hamburger hamburger--collapse">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
            </span>
            </span>
          </button>
          
        <div class="u-header__section u-header__section--dark g-bg-black-opacity-0_1 g-px-100" style="padding:1rem;">
         
            <!-- Logo -->
            <a href="/" class="navbar-brand">
              <img src="/frontend-assets/main-assets/assets/img/logo/logo-1.png" style="height:3rem" alt="Image Description">
            </a>
           
            <div class="d-inline-block g-hidden-xs-down g-pos-rel g-valign-middle g-pl-30 g-pl-0--lg" style="float:right">
                <a class="btn u-btn-outline-primary g-font-size-13 text-uppercase g-py-10 g-px-15" href="/login" ><i class="fa fa-user"></i>&nbsp; Login</a>
                <a class="btn u-btn-outline-primary g-font-size-13 text-uppercase g-py-10 g-px-15" href="#" id="menuToggler" ><i class="fa fa-bars"></i>&nbsp; Menu</a>
                
              </div>
          </div>
    </header> --}}
      <!-- Sidebar Navigation -->
      <div id="js-header" class="u-header u-header--side" aria-labelledby="header-toggler" data-header-behavior="overlay" data-header-position="right" data-header-breakpoint="lg" data-header-classes="g-transition-0_5" data-header-overlay-classes="g-bg-black-opacity-0_8 g-transition-0_5">
        <div class="u-header__sections-container g-bg-white g-brd-right--lg g-brd-gray-light-v5 g-py-10 g-py-10--lg g-px-14--lg">
          <div class="u-header__section u-header__section--light">
            <nav class="navbar navbar-expand-lg">
              <div class="js-mega-menu container">
                <!-- Responsive Toggle Button -->
              
            <button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-right-0" type="button" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
                <span class="hamburger hamburger--slider">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
                </span>
                </span>
              </button>
                <!-- End Responsive Toggle Button -->
                <!-- Logo -->
              
                <!-- End Logo -->
  
                <!-- Navigation -->

                <div class="collapse navbar-collapse align-items-center  w-100 g-mt-5 g-mt-0--lg g-mb-40" id="navBar">
                  <h3 class="menu_menu"> MENU</h3>
                   <hr style="background:black;width:100%;    margin: 1rem;">
                  <ul class="navbar-nav ml-auto text-uppercase g-font-weight-600 u-sub-menu-v1">
                    
                    <li class="nav-item g-my-5 menu_item">
                      <a href="/account" class="nav-link"><i class="fas fa-users"></i>&nbsp; ACCOUNT
                    
                        </a>
                    </li>
                    <li class="nav-item g-my-5 menu_item">
                      <a href="/signup" class="nav-link"><i class="fas fa-users"></i>&nbsp; SIGNUP
                    
                        </a>
                    </li>
                    <li class="nav-item g-my-5 menu_item">
                      <a href="/partnership" class="nav-link"><i class="fas fa-users"></i>&nbsp; PARTNERSHIP
                    
                        </a>
                    </li>
                    <li class="nav-item g-my-5 menu_item">
                      <a href="/login" class="nav-link"><i class="fas fa-users"></i>&nbsp; LOGIN
                    
                        </a>
                    </li>
                    <li class="nav-item g-my-5 menu_item">
                        <a href="/restaurants" class="nav-link"><i class="fas fa-bookmark"></i>&nbsp; RESTAURANTS
                      
                          </a>
                      </li>
                    <li class="nav-item g-my-5 menu_item">
                      <a href="/order-history" class="nav-link"><i class="fas fa-bookmark"></i>&nbsp; ORDER HISTORY
                    
                        </a>
                    </li>
                    <li class="nav-item g-my-5 menu_item">
                      <a href="/contact" class="nav-link"><i class="fas fa-bookmark"></i>&nbsp; CONTACT
                    
                        </a>
                    </li>
                    <li class="nav-item g-my-5 menu_item">
                      <a href="/terms-and-conditions" class="nav-link"><i class="fas fa-bookmark"></i>&nbsp; TERMS AND CONDITIONS
                    
                        </a>
                    </li>
                    <li class="nav-item g-my-5 menu_item">
                      <a href="#!" class="nav-link"><i class="fas fa-sign-out-alt"></i>&nbsp; LOGOUT
                    
                        </a>
                    </li>
                    
                   
                  </ul>
                </div>
                <!-- End Navigation -->
  
                <div class="text-center g-hidden-lg-down">
                  <ul class="list-inline g-mb-10">
                    <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
                      <a href="#!" class="g-color-main g-color-primary--hover">
                        <i class="fa fa-facebook"></i>
                      </a>
                    </li>
                    <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="" data-original-title="Instagram">
                      <a href="#!" class="g-color-main g-color-primary--hover">
                        <i class="fa fa-instagram"></i>
                      </a>
                    </li>
                    <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter">
                      <a href="#!" class="g-color-main g-color-primary--hover">
                        <i class="fa fa-twitter"></i>
                      </a>
                    </li>
                  </ul>
                  <p class="mb-0">2018 &copy; All Rights Reserved.</p>
                </div>
              </div>
            </nav>
          </div>
        </div>
      </div>
        <!-- End Sidebar Navigation -->
    
    <!-- End Header -->
