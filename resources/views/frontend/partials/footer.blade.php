<style>
  .footer_card{
    height: 17rem;
    background: #434848;
    padding: 1rem;
    border: 1px solid #434848;
    margin: 0 2rem;
  }
  .footer_card li h4{
        font-size: 18px;
  }
  .footer_card h2{
        font-size: 20px;
  }
  </style>
      <!-- Copyright Footer -->
      <div class="g-bg-black-opacity-0_9 g-color-white-opacity-0_8 g-py-60">
          <div class="container">
            <div class="row">
              <!-- Footer Content -->
              <div class="col-lg-4 col-md-6 g-mb-40 g-mb-0--lg">
                <div class="card footer_card">
                  <div class="u-heading-v2-3--bottom g-mb-10">
                    <h2 class="u-heading-v2__title h6 text-uppercase mb-0">Legal</h2>
                  </div> 
                  <nav class="text-uppercase1">
                      <ul class="list-unstyled g-mt-minus-10 mb-0">
                        <li class="g-pos-rel g-brd-bottom g-py-5">
                          <h4 class="h6 g-pr-20 mb-0">
                            <a class="g-color-white-opacity-0_8 g-color-white--hover" href="/terms-and-conditions">Terms and Conditions</a>
                            {{-- <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i> --}}
                          </h4>
                        </li>
                        <li class="g-pos-rel g-brd-bottom  g-py-5">
                          <h4 class="h6 g-pr-20 mb-0">
                            <a class="g-color-white-opacity-0_8 g-color-white--hover" href="/privacy-policy">Privacy Policy</a>
                            {{-- <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i> --}}
                          </h4>
                        </li>
                        <li class="g-pos-rel g-brd-bottom  g-py-5">
                          <h4 class="h6 g-pr-20 mb-0">
                            <a class="g-color-white-opacity-0_8 g-color-white--hover" href="/cookies">Cookies</a>
                            {{-- <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i> --}}
                          </h4>
                        </li>
                      </ul>
                    </nav>
                </div>
              </div>

              <!-- End Footer Content -->
        
              <!-- Footer Content -->
              <div class="col-lg-4 col-md-6 g-mb-40 g-mb-0--lg">
              
        
                <div class="card footer_card">
                  <div class="u-heading-v2-3--bottom g-mb-10">
                    <h2 class="u-heading-v2__title h6 text-uppercase mb-0">Help</h2>
                  </div> 
                  <nav class="text-uppercase1">
                      <ul class="list-unstyled g-mt-minus-10 mb-0">
                        <li class="g-pos-rel g-brd-bottom g-py-5">
                          <h4 class="h6 g-pr-20 mb-0">
                            <a class="g-color-white-opacity-0_8 g-color-white--hover" href="/contact-us">Contact</a>
                            {{-- <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i> --}}
                          </h4>
                        </li>
                        <li class="g-pos-rel g-brd-bottom g-py-5">
                          <h4 class="h6 g-pr-20 mb-0">
                            <a class="g-color-white-opacity-0_8 g-color-white--hover" href="/faqs">FAQs</a>
                            {{-- <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i> --}}
                          </h4>
                        </li>
                      </ul>
                    </nav>
                </div>
              </div>
              <!-- End Footer Content -->
       
              <!-- Footer Content -->
              <div class="col-lg-4 col-md-6">
                <div class="card footer_card">
                  <div class="u-heading-v2-3--bottom g-mb-20">
                    <h2 class="u-heading-v2__title h6 text-uppercase mb-0">Apps</h2>
                  </div> 
                  <a href="#!" style="margin-bottom:0.7rem" >
                  <img src="/img/appstore.png" style="height:4rem"/> 
                  </a>
                  
                  <a href="#!">
                  <img src="/img/playstore.png" style="height:4rem"/>
                  </a><br>

                </div>
              </div>
              <!-- End Footer Content -->
            </div>
          </div>
        </div>
        <!-- End Footer -->
        
      <footer class="g-bg-gray-dark-v1 g-color-white-opacity-0_8 g-py-20">
        <div class="container">
          <div class="row">
            <div class="col-md-8 text-center text-md-left g-mb-10 g-mb-0--md">
              <div class="d-lg-flex">
                <small class="d-block g-font-size-default g-mr-10 g-mb-10 g-mb-0--md">2017 © All Rights Reserved.</small>
                
              </div>
            </div>

            <div class="col-md-4 align-self-center">
              <ul class="list-inline text-center text-md-right mb-0">
                <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Facebook">
                  <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                    <i class="fa fa-facebook"></i>
                  </a>
                </li>
                <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Skype">
                  <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                    <i class="fa fa-skype"></i>
                  </a>
                </li>
                <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Linkedin">
                  <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                    <i class="fa fa-linkedin"></i>
                  </a>
                </li>
                <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Pinterest">
                  <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                    <i class="fa fa-pinterest"></i>
                  </a>
                </li>
                <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Twitter">
                  <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                    <i class="fa fa-twitter"></i>
                  </a>
                </li>
                <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Dribbble">
                  <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                    <i class="fa fa-dribbble"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
      <!-- End Copyright Footer -->
    </main>
  </body>

  <!-- JS Global Compulsory -->
  <script src="/frontend-assets/main-assets/assets/vendor/jquery/jquery.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/jquery.easing/js/jquery.easing.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/popper.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.js"></script>

  <script src="/frontend-assets/main-assets/assets/vendor/bootstrap/offcanvas.js"></script>


  <script  src="/frontend-assets/main-assets/assets/vendor/jquery-ui/jquery-ui.core.js"></script>

  <script  src="/frontend-assets/main-assets/assets/vendor/jquery-ui/ui/widgets/menu.js"></script>
  <script  src="/frontend-assets/main-assets/assets/vendor/jquery-ui/ui/widgets/mouse.js"></script>
  <script  src="/frontend-assets/main-assets/assets/vendor/jquery-ui/ui/widgets/autocomplete.js"></script>

  <!-- JS Unify -->
  <script  src="/frontend-assets/main-assets/assets/js/components/hs.autocomplete.js"></script>

  <!-- JS Implementing Plugins -->
  <script src="/frontend-assets/main-assets/assets/vendor/masonry/dist/masonry.pkgd.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/slick-carousel/slick/slick.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/fancybox/jquery.fancybox.min.js"></script>

  <script src="/frontend-assets/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>


  <script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>


  <!-- JS Unify -->
  <script src="/frontend-assets/main-assets/assets/js/hs.core.js"></script>

  <script src="/frontend-assets/main-assets/assets/js/components/hs.header-side.js"></script>
  <script src="/frontend-assets/main-assets/assets/js/components/hs.header.js"></script>
  {{-- <script src="/frontend-assets/main-assets/assets/js/components/hs.header.js"></script> --}}
  <script src="/frontend-assets/main-assets/assets/js/helpers/hs.hamburgers.js"></script>
  <script src="/frontend-assets/main-assets/assets/js/components/hs.scroll-nav.js"></script>
  <script src="/frontend-assets/main-assets/assets/js/components/hs.carousel.js"></script>





  @yield('js')

  <script>
    $(window).on('load', function () {
      // initialization of header
      $.HSCore.components.HSHeaderSide.init($('#js-header'));
      $.HSCore.components.HSHeader.init($('#js-header-top'));
      $.HSCore.helpers.HSHamburgers.init('.hamburger');

     
    });
    $(document).ready(function(){
      $('#menuToggler').click(function(){
        $('#header-toggler').trigger('click');
      })
    })
    </script>

</html>
